﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour
{
    static private Scorekeeper instance;

    public int pointsPerGoal = 1;
    private int[] score = new int[2];
    public Text[] scoreText;    public Text[] MainText;    public int scorePerGoal = 1;

    void Start()
    {
        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError(
            "More than one Scorekeeper exists in the scene.");
        }
        
        // reset the scores to zero
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }
    }

    public void OnScoreGoal(int player)
    {
        score[player] += scorePerGoal;
        scoreText[player].text = score[player].ToString();
        if (scoreText[1].text == "10")
        {
            Debug.Log("Player win");
            MainText[0].text = "Well done!";
            Time.timeScale = 0;
        }
        if (scoreText[0].text == "10")
        {
            Debug.Log("AI win");
            MainText[0].text = "That's one mean AI!";
            Time.timeScale = 0;
        }
    }

    static public Scorekeeper Instance
    {
        get { return instance; }
    }

}
